//
// Created by Mauro on 04/10/2019.
//

#include "File.h"

void File::write(const std::string& text) {
    outfile.open(fileName, std::ios_base::trunc);
    outfile << text;
    outfile.close();
}


File::File(std::string fileName) : fileName{std::move(fileName)} {
    write("");
}

void File::append(const std::string& text, bool autoAddNextLine) {
    outfile.open(fileName, std::ios_base::app);
    outfile << text ;
    if ( autoAddNextLine ) outfile << "\n";
    outfile.close();
}

std::string File::read() {

    std::string tmp;
    std::string text;
    inFile.open(fileName, std::ios_base::in);

    while (std::getline(inFile, tmp))
    {
        text.append(tmp + "\n");
    }

    inFile.close();

    return text;

}

std::string File::formatLine(std::vector<std::string> words, const std::string& delimiter) {

    std::string formatted;

    for(auto i = 0ULL; i < words.size(); i++){
        formatted.append(words.at(i) );
        if ( i < words.size() - 1) formatted.append(delimiter);
    }

    return formatted;
}

std::vector<std::string> File::getLines() {

    std::vector<std::string> lines;

    forEachLine([&lines] (const std::string& line){lines.push_back(line); });

    return lines;
}

__int64 File::getNumOfLines() {
    std::string line;
    __int64 n = 0;
    inFile.open(fileName);
    while (std::getline(inFile, line)) {
        n++;
    }
    inFile.close();
    return n;
}

int File::getNumOfWords() {
    int count = 0;
    inFile.open(fileName);
    for (std::string word; inFile >> word; ++count) {}
    inFile.close();
    return count;
}

__int64 File::countWordOccurrences(const std::string& word) {

    __int64 count = 0;
    std::string tmp;
    inFile.open(fileName);
    while(inFile >> tmp){
        if (tmp == word) count++;
    }

    inFile.close();
    return count;
}

std::vector<__int64> File::getWordPos(std::string word) {

    __int64 count = 0;
    std::vector<__int64> linesNumber;
    forEachLine([&linesNumber, &count, &word]
    (const std::string& line){

        std::istringstream ss(line);
        do {
            std::string tmp;
            ss >> tmp;

            if ( tmp == word){
                linesNumber.push_back(count);
            }
        } while (ss);

        count++;
    });

    return linesNumber;
}

void File::write(const std::vector<std::string>& lines) {

    for (const auto& line : lines){
        append(line, true);
    }

}

