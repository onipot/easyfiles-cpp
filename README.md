The binaries you'll find in the _bin_ folder have been compiled on a Windows 10 x64 machine using MSVC 2019, hence if you're running a different version of Windows, consider to rebuild the project.

# HOW TO INTEGRATE 

- Download the project
- Edit CMakeLists.txt
- (Only for shared library) Copy the .dll in the same folder of your .exe

## CMakeLists.txt
Your CMakeLists.txt should look like this below:
~~~~
cmake_minimum_required(VERSION 3.13)
project(yourProjectName)

set(CMAKE_CXX_STANDARD 17)

INCLUDE_DIRECTORIES("YourEasyFilesDllFolder\\sources")

add_executable(yourProjectName main.cpp)
target_link_libraries(yourProjectName YourPathToTheStaticOrSharedLib/EasyFilesDll.lib)`
~~~~

## Code example
In your main.cpp you'll end up with something like:


    #include "File.h"
    
    int main(){
        
        File f("myFile.txt");
        // your code
    }

