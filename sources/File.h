//
// Created by Mauro on 04/10/2019.
//

#ifndef EASYFILES_FILE_H
#define EASYFILES_FILE_H

#define DLL __declspec(dllexport)
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
class File {
private:
    std::string fileName;
    std::ofstream outfile;
    std::ifstream inFile;

public:
    DLL std::string getFileName(){
        return fileName;
    }

    DLL explicit File(std::string fileName);

    DLL void write(const std::string& text);
    DLL void write(const std::vector<std::string>& lines);

    DLL static std::string formatLine(std::vector<std::string> words, const std::string& delimiter);

    DLL void append(const std::string& text, bool autoAddNextLine);

    DLL std::string read();

    DLL std::vector<std::string> getLines();

    DLL __int64 getNumOfLines();

    DLL int getNumOfWords();

    DLL __int64 countWordOccurrences(const std::string& word);

    DLL std::vector<__int64> getWordPos(std::string word);
    template<typename Functor>
    DLL void forEachLine(Functor runAtEachLine){
        std::string line;
        inFile.open(fileName, std::ios_base::in);

        while (std::getline(inFile, line))
        {
            runAtEachLine(line);
        }

        inFile.close();
    }

};


#endif //EASYFILES_FILE_H
